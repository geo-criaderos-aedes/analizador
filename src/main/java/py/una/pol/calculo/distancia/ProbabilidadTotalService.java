package py.una.pol.calculo.distancia;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ProbabilidadTotalService {

    private static ProbabilidadTotalService instance;

    private static final int RADIO_MTS = 400;

    private ProbabilidadTotalService() {

    }

    public static ProbabilidadTotalService getInstance() {
        if (instance == null) {
            instance = new ProbabilidadTotalService();
        }
        return instance;
    }

    private List<PuntoGeografico> listarFebriles() {
        List<PuntoGeografico> listaFebriles = new ArrayList<PuntoGeografico>();
        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet resultsetFebriles = null;
        try {
            ComboPooledDataSource dataSource
                    = DatabaseConfig.getInstance().getDatasource();
            connection = dataSource.getConnection();

            StringBuilder selectSQL = new StringBuilder();
            selectSQL.append("SELECT distinct(latitud), longitud FROM registro_s1 ");
            selectSQL.append("WHERE febriles > 0 ");
            selectSQL.append("AND id > 92 AND id < 4239 ");
            selectSQL.append("AND estado = 'ACTIVO' ");
            selectSQL.append("AND latitud IS NOT NULL ");
            selectSQL.append("AND longitud IS NOT NULL");

            pstm = connection.prepareStatement(selectSQL.toString());

            resultsetFebriles = pstm.executeQuery();

            while (resultsetFebriles.next()) {
                double lonFebriles = resultsetFebriles.getDouble("longitud");
                double latFebriles = resultsetFebriles.getDouble("latitud");
                listaFebriles.add(new PuntoGeografico(lonFebriles, latFebriles));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return listaFebriles;
    }

    public void executeService() {
        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet resultsetCriaderoA = null;
        ResultSet resultsetCriaderoB = null;
        ResultSet resultsetCriaderoC = null;
        try {
            ComboPooledDataSource dataSource
                    = DatabaseConfig.getInstance().getDatasource();
            connection = dataSource.getConnection();

            StringBuilder selectSQL = new StringBuilder();
            selectSQL.append("SELECT distinct(latitud), longitud, criaderos_a1 a1, ");
            selectSQL.append("criaderos_a2 a2, criaderos_a3 a3, criaderos_a4 a4, ");
            selectSQL.append("criaderos_a5 a5, criaderos_a6 a6, criaderos_a7 a7, ");
            selectSQL.append("criaderos_a8 a8, criaderos_a9 a9, criaderos_a10 a10 ");
            selectSQL.append("FROM registro_s1 r, criadero_a c ");
            selectSQL.append("WHERE c.fk_registro_s1 = r.id ");
            selectSQL.append("AND r.sin_criaderos = false ");
            selectSQL.append("AND r.id > 92 AND r.id < 4239 ");
            selectSQL.append("AND r.estado = 'ACTIVO' ");
            selectSQL.append("AND r.latitud IS NOT NULL ");
            selectSQL.append("AND r.longitud IS NOT NULL");

            pstm = connection.prepareStatement(selectSQL.toString());

            resultsetCriaderoA = pstm.executeQuery();

            List<PuntoGeografico> listaTotalCriaderos = new ArrayList<PuntoGeografico>();
            while (resultsetCriaderoA.next()) {
                double longitud = resultsetCriaderoA.getDouble("longitud");
                double latitud = resultsetCriaderoA.getDouble("latitud");

                Integer a1 = resultsetCriaderoA.getInt("a1");
                Integer a2 = resultsetCriaderoA.getInt("a2");
                Integer a3 = resultsetCriaderoA.getInt("a3");
                Integer a4 = resultsetCriaderoA.getInt("a4");
                Integer a5 = resultsetCriaderoA.getInt("a5");
                Integer a6 = resultsetCriaderoA.getInt("a6");
                Integer a7 = resultsetCriaderoA.getInt("a7");
                Integer a8 = resultsetCriaderoA.getInt("a8");
                Integer a9 = resultsetCriaderoA.getInt("a9");
                Integer a10 = resultsetCriaderoA.getInt("a10");

                PuntoGeografico puntoGeografico = new PuntoGeografico(longitud, latitud);
                puntoGeografico.setA1(a1);
                puntoGeografico.setA2(a2);
                puntoGeografico.setA3(a3);
                puntoGeografico.setA4(a4);
                puntoGeografico.setA5(a5);
                puntoGeografico.setA6(a6);
                puntoGeografico.setA7(a7);
                puntoGeografico.setA8(a8);
                puntoGeografico.setA9(a9);
                puntoGeografico.setA10(a10);

                listaTotalCriaderos.add(puntoGeografico);
            }

            selectSQL = new StringBuilder();
            selectSQL.append("SELECT distinct(latitud), longitud, criaderos_b1 b1, ");
            selectSQL.append("criaderos_b2 b2, criaderos_b3 b3, criaderos_b4 b4, ");
            selectSQL.append("criaderos_b5 b5, criaderos_b6 b6 ");
            selectSQL.append("FROM registro_s1 r, criadero_b c ");
            selectSQL.append("WHERE c.fk_registro_s1 = r.id ");
            selectSQL.append("AND r.sin_criaderos = false ");
            selectSQL.append("AND r.id > 92 AND r.id < 4239 ");
            selectSQL.append("AND r.estado = 'ACTIVO' ");
            selectSQL.append("AND r.latitud IS NOT NULL ");
            selectSQL.append("AND r.longitud IS NOT NULL");

            pstm = connection.prepareStatement(selectSQL.toString());

            resultsetCriaderoB = pstm.executeQuery();

            while (resultsetCriaderoB.next()) {
                double longitud = resultsetCriaderoB.getDouble("longitud");
                double latitud = resultsetCriaderoB.getDouble("latitud");

                Integer b1 = resultsetCriaderoB.getInt("b1");
                Integer b2 = resultsetCriaderoB.getInt("b2");
                Integer b3 = resultsetCriaderoB.getInt("b3");
                Integer b4 = resultsetCriaderoB.getInt("b4");
                Integer b5 = resultsetCriaderoB.getInt("b5");
                Integer b6 = resultsetCriaderoB.getInt("b6");

                PuntoGeografico puntoGeografico = new PuntoGeografico(longitud, latitud);
                puntoGeografico.setB1(b1);
                puntoGeografico.setB2(b2);
                puntoGeografico.setB3(b3);
                puntoGeografico.setB4(b4);
                puntoGeografico.setB5(b5);
                puntoGeografico.setB6(b6);

                listaTotalCriaderos.add(puntoGeografico);
            }

            selectSQL = new StringBuilder();
            selectSQL.append("SELECT distinct(latitud), longitud, criaderos_c1 c1, ");
            selectSQL.append("criaderos_c2 c2, criaderos_c3 c3, criaderos_c4 c4 ");
            selectSQL.append("FROM registro_s1 r, criadero_c c ");
            selectSQL.append("WHERE c.fk_registro_s1 = r.id ");
            selectSQL.append("AND r.sin_criaderos = false ");
            selectSQL.append("AND r.id > 92 AND r.id < 4239 ");
            selectSQL.append("AND r.estado = 'ACTIVO' ");
            selectSQL.append("AND r.latitud IS NOT NULL ");
            selectSQL.append("AND r.longitud IS NOT NULL");

            pstm = connection.prepareStatement(selectSQL.toString());

            resultsetCriaderoC = pstm.executeQuery();

            while (resultsetCriaderoC.next()) {
                double longitud = resultsetCriaderoC.getDouble("longitud");
                double latitud = resultsetCriaderoC.getDouble("latitud");

                Integer c1 = resultsetCriaderoC.getInt("c1");
                Integer c2 = resultsetCriaderoC.getInt("c2");
                Integer c3 = resultsetCriaderoC.getInt("c3");
                Integer c4 = resultsetCriaderoC.getInt("c4");

                PuntoGeografico puntoGeografico = new PuntoGeografico(longitud, latitud);
                puntoGeografico.setC1(c1);
                puntoGeografico.setC2(c2);
                puntoGeografico.setC3(c3);
                puntoGeografico.setC4(c4);

                listaTotalCriaderos.add(puntoGeografico);
            }

            List<PuntoGeografico> listaFebriles = listarFebriles();
            for (PuntoGeografico puntoFebril : listaFebriles) {
                PuntoGeografico centro = new PuntoGeografico();
                centro.setLatitud(puntoFebril.getLatitud());
                centro.setLongitud(puntoFebril.getLongitud());

                Integer[] arrayA = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                Integer[] arrayB = {0, 0, 0, 0, 0, 0};
                Integer[] arrayC = {0, 0, 0, 0};

                for (PuntoGeografico tot : listaTotalCriaderos) {
                    double distanciaKm = Haversine.distance(centro.getLatitud(), centro.getLongitud(), tot.getLatitud(), tot.getLongitud());
                    double distanciaM = distanciaKm * 1000;
                    if (distanciaM <= RADIO_MTS && distanciaM > 0) {
                        arrayA[0] += tot.getA1() != null ? tot.getA1() : 0;
                        arrayA[1] += tot.getA2() != null ? tot.getA2() : 0;
                        arrayA[2] += tot.getA3() != null ? tot.getA3() : 0;
                        arrayA[3] += tot.getA4() != null ? tot.getA4() : 0;
                        arrayA[4] += tot.getA5() != null ? tot.getA5() : 0;
                        arrayA[5] += tot.getA6() != null ? tot.getA6() : 0;
                        arrayA[6] += tot.getA7() != null ? tot.getA7() : 0;
                        arrayA[7] += tot.getA8() != null ? tot.getA8() : 0;
                        arrayA[8] += tot.getA9() != null ? tot.getA9() : 0;
                        arrayA[9] += tot.getA10() != null ? tot.getA10() : 0;

                        arrayB[0] += tot.getB1() != null ? tot.getB1() : 0;
                        arrayB[1] += tot.getB2() != null ? tot.getB2() : 0;
                        arrayB[2] += tot.getB3() != null ? tot.getB3() : 0;
                        arrayB[3] += tot.getB4() != null ? tot.getB4() : 0;
                        arrayB[4] += tot.getB5() != null ? tot.getB5() : 0;
                        arrayB[5] += tot.getB6() != null ? tot.getB6() : 0;

                        arrayC[0] += tot.getC1() != null ? tot.getC1() : 0;
                        arrayC[1] += tot.getC2() != null ? tot.getC2() : 0;
                        arrayC[2] += tot.getC3() != null ? tot.getC3() : 0;
                        arrayC[3] += tot.getC4() != null ? tot.getC4() : 0;
                    }
                }

                Integer total = 0;
                for (int i = 0; i < arrayA.length; i++) {
                    total += arrayA[i];
                    System.out.println("Total A" + (i + 1) + ": " + arrayA[i]);
                }
                for (int i = 0; i < arrayB.length; i++) {
                    total += arrayB[i];
                    System.out.println("Total B" + (i + 1) + ": " + arrayB[i]);
                }
                for (int i = 0; i < arrayC.length; i++) {
                    total += arrayC[i];
                    System.out.println("Total C" + (i + 1) + ": " + arrayC[i]);
                }

                BigDecimal[] arrayProbA = new BigDecimal[arrayA.length];
                BigDecimal[] arrayProbB = new BigDecimal[arrayB.length];
                BigDecimal[] arrayProbC = new BigDecimal[arrayC.length];

                for (int i = 0; i < arrayA.length; i++) {
                    arrayProbA[i] = new BigDecimal(arrayA[i]).divide(new BigDecimal(total), 2, RoundingMode.HALF_UP);
                    System.out.println("Porcentaje A" + (i + 1) + ": " + arrayProbA[i]);
                }
                for (int i = 0; i < arrayB.length; i++) {
                    arrayProbB[i] = new BigDecimal(arrayB[i]).divide(new BigDecimal(total), 2, RoundingMode.HALF_UP);
                    System.out.println("Porcentaje B" + (i + 1) + ": " + arrayProbB[i]);
                }
                for (int i = 0; i < arrayC.length; i++) {
                    arrayProbC[i] = new BigDecimal(arrayC[i]).divide(new BigDecimal(total), 2, RoundingMode.HALF_UP);
                    System.out.println("Porcentaje C" + (i + 1) + ": " + arrayProbC[i]);
                }

                BigDecimal[] arrayProbTotal = new BigDecimal[20];

                arrayProbTotal[0] = arrayProbA[0].multiply(new BigDecimal(PuntoGeografico.ra1));
                arrayProbTotal[1] = arrayProbA[1].multiply(new BigDecimal(PuntoGeografico.ra2));
                arrayProbTotal[2] = arrayProbA[2].multiply(new BigDecimal(PuntoGeografico.ra3));
                arrayProbTotal[3] = arrayProbA[3].multiply(new BigDecimal(PuntoGeografico.ra4));
                arrayProbTotal[4] = arrayProbA[4].multiply(new BigDecimal(PuntoGeografico.ra5));
                arrayProbTotal[5] = arrayProbA[5].multiply(new BigDecimal(PuntoGeografico.ra6));
                arrayProbTotal[6] = arrayProbA[6].multiply(new BigDecimal(PuntoGeografico.ra7));
                arrayProbTotal[7] = arrayProbA[7].multiply(new BigDecimal(PuntoGeografico.ra8));
                arrayProbTotal[8] = arrayProbA[8].multiply(new BigDecimal(PuntoGeografico.ra9));
                arrayProbTotal[9] = arrayProbA[9].multiply(new BigDecimal(PuntoGeografico.ra10));

                arrayProbTotal[10] = arrayProbB[0].multiply(new BigDecimal(PuntoGeografico.rb1));
                arrayProbTotal[11] = arrayProbB[1].multiply(new BigDecimal(PuntoGeografico.rb2));
                arrayProbTotal[12] = arrayProbB[2].multiply(new BigDecimal(PuntoGeografico.rb3));
                arrayProbTotal[13] = arrayProbB[3].multiply(new BigDecimal(PuntoGeografico.rb4));
                arrayProbTotal[14] = arrayProbB[4].multiply(new BigDecimal(PuntoGeografico.rb5));
                arrayProbTotal[15] = arrayProbB[5].multiply(new BigDecimal(PuntoGeografico.rb6));

                arrayProbTotal[16] = arrayProbC[0].multiply(new BigDecimal(PuntoGeografico.rc1));
                arrayProbTotal[17] = arrayProbC[1].multiply(new BigDecimal(PuntoGeografico.rc2));
                arrayProbTotal[18] = arrayProbC[2].multiply(new BigDecimal(PuntoGeografico.rc3));
                arrayProbTotal[19] = arrayProbC[3].multiply(new BigDecimal(PuntoGeografico.rc4));

                BigDecimal probTotal = BigDecimal.ZERO;

                for (int i = 0; i < arrayProbTotal.length; i++) {
                    probTotal = probTotal.add(arrayProbTotal[i]);
                    System.out.println("arrayProbTotal: " + arrayProbTotal[i].setScale(3, RoundingMode.HALF_UP));
                }

                System.out.println("Punto febril en longitud: " + centro.getLongitud() + " y latitud: " + centro.getLatitud());
                System.out.println("Probabilidad Total: " + probTotal.setScale(3, RoundingMode.HALF_UP));

                for (int i = 0; i <= 9; i++) {
                    String res = arrayProbTotal[i].divide(probTotal, 3, RoundingMode.HALF_UP).toString();
                    System.out.println("Probabilidad A" + (i + 1) + ": " + res);
                }
                for (int i = 10; i <= 15; i++) {
                    String res = arrayProbTotal[i].divide(probTotal, 3, RoundingMode.HALF_UP).toString();
                    System.out.println("Probabilidad B" + (i - 9) + ": " + res);
                }
                for (int i = 16; i <= 19; i++) {
                    String res = arrayProbTotal[i].divide(probTotal, 3, RoundingMode.HALF_UP).toString();
                    System.out.println("Probabilidad C" + (i - 15) + ": " + res);
                }
                System.out.println("*****************************************");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
