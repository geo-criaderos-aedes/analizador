package py.una.pol.calculo.distancia;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;

public class DatabaseConfig {

    private static DatabaseConfig instance;
    private static ComboPooledDataSource cpds;

    private DatabaseConfig() {

    }

    public static DatabaseConfig getInstance() {
        if (instance == null) {
            instance = new DatabaseConfig();
        }
        return instance;
    }

    public ComboPooledDataSource getDatasource() throws PropertyVetoException {
        if (cpds == null) {
            cpds = new ComboPooledDataSource();
            cpds.setDriverClass(PropertiesConfig.getInstance().getDriverClass());
            cpds.setJdbcUrl(PropertiesConfig.getInstance().getJdbcUrl());
            cpds.setUser(PropertiesConfig.getInstance().getUser());
            cpds.setPassword(PropertiesConfig.getInstance().getPassword());
        }
        return cpds;
    }
}
