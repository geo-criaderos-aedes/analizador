package py.una.pol.calculo.distancia;

public class Main {

    public static void main(String[] args) throws Exception {
        PropertiesConfig.getInstance().loadProperties();
        ProbabilidadTotalService.getInstance().executeService();
    }
}
