package py.una.pol.calculo.distancia;

public class PuntoGeografico {

    private double longitud;
    private double latitud;

    private Integer A1;
    private Integer A2;
    private Integer A3;
    private Integer A4;
    private Integer A5;
    private Integer A6;
    private Integer A7;
    private Integer A8;
    private Integer A9;
    private Integer A10;

    private Integer B1;
    private Integer B2;
    private Integer B3;
    private Integer B4;
    private Integer B5;
    private Integer B6;

    private Integer C1;
    private Integer C2;
    private Integer C3;
    private Integer C4;

    public static double ra1 = 0.5;
    public static double ra2 = 0.7;
    public static double ra3 = 0.7;
    public static double ra4 = 0.7;
    public static double ra5 = 0.7;
    public static double ra6 = 0.3;
    public static double ra7 = 0.7;
    public static double ra8 = 0.5;
    public static double ra9 = 0.5;
    public static double ra10 = 0.7;

    public static double rb1 = 0.7;
    public static double rb2 = 0.7;
    public static double rb3 = 0.7;
    public static double rb4 = 0.7;
    public static double rb5 = 0.5;
    public static double rb6 = 0.7;

    public static double rc1 = 0.3;
    public static double rc2 = 0.3;
    public static double rc3 = 0.3;
    public static double rc4 = 0.3;

    public PuntoGeografico() {
    }

    public PuntoGeografico(double longitud, double latitud) {
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public Integer getA1() {
        return A1;
    }

    public void setA1(Integer A1) {
        this.A1 = A1;
    }

    public Integer getA2() {
        return A2;
    }

    public void setA2(Integer A2) {
        this.A2 = A2;
    }

    public Integer getA3() {
        return A3;
    }

    public void setA3(Integer A3) {
        this.A3 = A3;
    }

    public Integer getA4() {
        return A4;
    }

    public void setA4(Integer A4) {
        this.A4 = A4;
    }

    public Integer getA5() {
        return A5;
    }

    public void setA5(Integer A5) {
        this.A5 = A5;
    }

    public Integer getA6() {
        return A6;
    }

    public void setA6(Integer A6) {
        this.A6 = A6;
    }

    public Integer getA7() {
        return A7;
    }

    public void setA7(Integer A7) {
        this.A7 = A7;
    }

    public Integer getA8() {
        return A8;
    }

    public void setA8(Integer A8) {
        this.A8 = A8;
    }

    public Integer getA9() {
        return A9;
    }

    public void setA9(Integer A9) {
        this.A9 = A9;
    }

    public Integer getA10() {
        return A10;
    }

    public void setA10(Integer A10) {
        this.A10 = A10;
    }

    public Integer getB1() {
        return B1;
    }

    public void setB1(Integer B1) {
        this.B1 = B1;
    }

    public Integer getB2() {
        return B2;
    }

    public void setB2(Integer B2) {
        this.B2 = B2;
    }

    public Integer getB3() {
        return B3;
    }

    public void setB3(Integer B3) {
        this.B3 = B3;
    }

    public Integer getB4() {
        return B4;
    }

    public void setB4(Integer B4) {
        this.B4 = B4;
    }

    public Integer getB5() {
        return B5;
    }

    public void setB5(Integer B5) {
        this.B5 = B5;
    }

    public Integer getB6() {
        return B6;
    }

    public void setB6(Integer B6) {
        this.B6 = B6;
    }

    public Integer getC1() {
        return C1;
    }

    public void setC1(Integer C1) {
        this.C1 = C1;
    }

    public Integer getC2() {
        return C2;
    }

    public void setC2(Integer C2) {
        this.C2 = C2;
    }

    public Integer getC3() {
        return C3;
    }

    public void setC3(Integer C3) {
        this.C3 = C3;
    }

    public Integer getC4() {
        return C4;
    }

    public void setC4(Integer C4) {
        this.C4 = C4;
    }
}
