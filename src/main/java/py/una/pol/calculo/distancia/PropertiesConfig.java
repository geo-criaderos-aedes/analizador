package py.una.pol.calculo.distancia;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfig {

    private static PropertiesConfig instance;

    private String driverClass;
    private String jdbcUrl;
    private String user;
    private String password;

    private PropertiesConfig() {

    }

    public static PropertiesConfig getInstance() {
        if (instance == null) {
            instance = new PropertiesConfig();
        }
        return instance;
    }

    public void loadProperties() {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            String filePath = System.getProperty("user.dir");
            input = new FileInputStream(filePath + "/etc/config.properties");

            prop.load(input);

            driverClass = prop.getProperty("driverClass");
            jdbcUrl = prop.getProperty("jdbcUrl");
            user = prop.getProperty("user");
            password = prop.getProperty("password");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
